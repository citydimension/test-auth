﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace TestJwt.Controllers
{
    [ApiController]
    public class TestController : ControllerBase
    {
        [Route("")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public ActionResult<string> Get()
        {
            string userInfo =
                User != null ? string.Join(Environment.NewLine, User.Claims.Select(c => $"{c.Type}:{c.Value}"))
                             : string.Empty;
            return "test successfull:" + userInfo;
        }
    }
}
