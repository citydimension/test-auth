﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace TestJwt
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddAuthorization();

            services.AddAuthentication()
                    .AddJwtBearer(
                        JwtBearerDefaults.AuthenticationScheme, 
                        o =>
                        {
                            o.Authority = Configuration["OpenIdAuthority"];
                            o.Audience = Configuration["OpenIdAudience"];
                            o.SaveToken = true;
                        });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseAuthentication()
               .UseStatusCodePages()
               .UseMvc();
        }
    }
}
